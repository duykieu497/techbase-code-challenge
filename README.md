# Covid 19 Interview App - KIEUDUY

This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

> For realtime feature, switch to `optimized` branch

## Logics which still missing

- Messaging (error message, info message,...)

These logics will be optimized on branch `optimize`, you should check it out.

## Available Scripts

In the project directory, you can run:


### `npm install`

Installing all dependencies

### `npm run build && npm install -g live-server && cd build && live-server`

Quick running production mode
Open [http://localhost:8080](http://localhost:8080) to view it in the browser.

### `npm start`

Runs the app in the development mode.\
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.\
You will also see any lint errors in the console.

### `cypress open`

This project does not write test in unit test, instead I write e2e by using cypress.

After Cypress opened, click on `covid.spec.js` for running the test.

You must run project first in development mode or edit `cypress.json` to change test URL 