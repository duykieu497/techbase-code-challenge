describe("Testing the home page", () => {
    before(() => {
        cy.visit("/");
    });

    it("should render navbar", () => {
        cy.get("#navMenu li").should("have.length.gte", 1);
    });

    it("should render home page global summary information", () => {
        cy.get("#globalSummary").should("have.length.gte", 1);
    });

    it("should render countries list", () => {
        cy.get(".country-list-item").should("have.length.gte", 10);
    });

    it("the search should be functionality", () => {
        cy.get("#searchToggle").click();
        cy.get("#searchCountries").type("US{downArrow}{enter}");
    });

    it("should render country summary information", () => {
        cy.get("#globalSummary").should("have.length.gte", 1);
    });

    it("should render two chart data", () => {
        cy.get("#allDataChart").should("have.length.gte", 1);
        cy.get("#last30DaysChart").should("have.length.gte", 1);
    });
});
