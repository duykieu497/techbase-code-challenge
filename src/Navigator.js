import React from "react";
import { HashRouter, Switch, Route, Link } from "react-router-dom";
import Layout from "./components/UI/Layout";

// Routes
import HealthInfo from "./views/HealthInfo";
import Home from "./views/Home";
import FAQ from "./views/FAQ";
import Country from "./views/Country";

const Navigator = () => (
    <HashRouter>
        <Switch>
            <Route exact path="/" component={Home} />
            <Route path="/health-info" component={HealthInfo} />
            <Route path="/faqs" component={FAQ} />
            <Route path="/country/:code" component={Country} />
        </Switch>
    </HashRouter>
);

export default Navigator;
