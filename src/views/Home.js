import React from "react";
import { connect } from "react-redux";
import Layout from "../components/UI/Layout";
import { detectScrollBottom } from "../utils";
import WipChart from "../components/WipChart";
import Summary from "../components/Summary";
import CountryListItem from "../components/CountryListItem";

const mapStateToProps = ({ countriesSummary, globalSummary, loading, wip }) => ({
    countriesSummary,
    globalSummary,
    loading,
    wip,
});

const Home = connect(mapStateToProps)(({ countriesSummary, globalSummary, wip, loading }) => {
    // Only show a limit of countries
    const [displayCountries, setDisplay] = React.useState([]);
    // State used for detecting is current scroll is the end of page
    const [isBottom, setIsBottom] = React.useState(false);
    // Search keyword
    const [keywords, setKeywords] = React.useState("");
    /**
     * Limit first render to 30
     */
    React.useEffect(() => {
        if (countriesSummary.length && !displayCountries.length) {
            setDisplay(countriesSummary.slice(0, 30));
        }
    }, [countriesSummary, displayCountries]);

    /**
     * Track window scrolling for lazy rendering
     */
    const handleScroll = () => {
        detectScrollBottom(() => {
            setIsBottom(true);
        });
    };

    /**
     * Listen to scroll
     */
    React.useEffect(() => {
        window.addEventListener("scroll", handleScroll);

        return () => {
            window.removeEventListener("scroll", handleScroll);
        };
    }, []);

    /**
     * Perform update
     */
    React.useEffect(() => {
        if (isBottom && displayCountries.length < countriesSummary.length) {
            setDisplay((curr) => [
                ...curr,
                ...countriesSummary.slice(curr.length, curr.length + 10),
            ]);
            setIsBottom(false);
        }
    }, [isBottom, countriesSummary, displayCountries]);

    const onSearch = (a) => (b) => {
        console.log(a, b);
    };

    const renderListCountries = () => {
        if (!Array.isArray(displayCountries)) return null;
        if (!displayCountries.length) return null;
        return (
            <React.Fragment>
                <h2>By countries</h2>
                <div className="row">
                    {displayCountries.map((country) => (
                        <CountryListItem key={country.Country} country={country} />
                    ))}
                </div>
            </React.Fragment>
        );
    };

    return (
        <Layout loading={loading} search={onSearch}>
            <Summary title={"Global summary New/Total"} data={globalSummary} />
            {/*<WipChart wipData={wip} title="World statistics" />*/}
            {renderListCountries()}
        </Layout>
    );
});

export default Home;
