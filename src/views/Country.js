import React from "react";
import { useRouteMatch } from "react-router-dom";
import { connect } from "react-redux";
import { Card } from "antd";
import Layout from "../components/UI/Layout";
import { fetchCountryData } from "../redux/actions";
import Summary from "../components/Summary";
import WipChart from "../components/WipChart";

const mapStateToProps = (state) => ({
    currentCountriesData: state.currentCountriesData,
    countriesSummary: state.countriesSummary,
    countries: state.countries,
});

const Country = ({ currentCountriesData, countriesSummary, countries, dispatch }) => {
    const {
        params: { code },
    } = useRouteMatch("/country/:code");

    const [countrySummary, setCountrySummary] = React.useState({});

    React.useEffect(() => {
        const foundCountry = countriesSummary.find((item) => item.CountryCode === code);
        setCountrySummary(foundCountry);

        // Fetching more data
        if (countries[code]) {
            dispatch(fetchCountryData(countries[code].Slug));
        }

        // Find current countries summary
    }, [countriesSummary, code]);

    if (!countries[code]) {
        return <h1>Sorry! Your country search is invalid</h1>;
    }

    return (
        <Layout>
            {currentCountriesData && countries[code] && (
                <React.Fragment>
                    <Summary title={`${countries[code].Country} New/Total`} data={countrySummary} />
                    {Array.isArray(currentCountriesData) && (
                        <React.Fragment>
                            <Card
                                id="allDataChart"
                                title="All data"
                                style={{ marginBottom: "2rem" }}
                            >
                                <WipChart wipData={currentCountriesData} />
                            </Card>
                            <Card id="last30DaysChart" title="Last 30 days data">
                                {currentCountriesData.length - 31}
                                <WipChart
                                    wipData={currentCountriesData.slice(
                                        currentCountriesData.length - 31,
                                        currentCountriesData.length - 1
                                    )}
                                />
                            </Card>
                        </React.Fragment>
                    )}
                </React.Fragment>
            )}
        </Layout>
    );
};

export default connect(mapStateToProps)(Country);
