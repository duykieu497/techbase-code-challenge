import React from "react";
import { Collapse, Tabs, Card } from "antd";
import Layout from "../components/UI/Layout";

const { Panel } = Collapse;
const { TabPane } = Tabs;

const HealthInfo = () => {
    const [active, setActive] = React.useState(1);

    const handleChange = ([, value]) => {
        setActive(Number(value));
    };

    return (
        <Layout>
            <Tabs defaultActiveKey="1">
                <TabPane tab="Symptoms" key="1">
                    <Card>
                        <p>The most common symptoms of COVID-19 are</p>
                        <ul>
                            <li>Fever</li>
                            <li>Dry cough</li>
                            <li>Fatigue</li>
                        </ul>
                        <p>
                            Other symptoms that are less common and may affect some patients
                            include:
                        </p>
                        <ul>
                            <li>Loss of taste or smell,</li>
                            <li>Nasal congestion,</li>
                            <li>Conjunctivitis (also known as red eyes)</li>
                            <li>Sore throat,</li>
                            <li>Headache,</li>
                            <li>Muscle or joint pain,</li>
                            <li>Different types of skin rash,</li>
                            <li>Nausea or vomiting,</li>
                            <li>Diarrhea,</li>
                            <li>Chills or dizziness.</li>
                        </ul>
                        <p>
                            Symptoms are usually mild. Some people become infected but only have
                            very mild symptoms or none at all.
                        </p>
                        <p>Symptoms of severe COVID‐19 disease include:</p>
                        <ul>
                            <li>Shortness of breath,</li>
                            <li>Loss of appetite,</li>
                            <li>Confusion,</li>
                            <li>Persistent pain or pressure in the chest,</li>
                            <li>High temperature (above 38 &deg;C).</li>
                        </ul>
                        <p>Other less common symptoms are:</p>
                        <ul>
                            <li>Irritability,</li>
                            <li>Confusion,</li>
                            <li>Reduced consciousness (sometimes associated with seizures),</li>
                            <li>Anxiety,</li>
                            <li>Depression,</li>
                            <li>Sleep disorders,</li>
                            <li>
                                More severe and rare neurological complications such as strokes,
                                brain inflammation, delirium and nerve damage.
                            </li>
                        </ul>
                        <p>
                            People of all ages who experience fever and/or cough associated with
                            difficulty breathing or shortness of breath, chest pain or pressure, or
                            loss of speech or movement should seek medical care immediately. If
                            possible, call your health care provider, hotline or health facility
                            first, so you can be directed to the right clinic.
                        </p>
                    </Card>
                </TabPane>
                <TabPane tab="Prevention" key="2">
                    <Card>
                        <p>
                            If you have been exposed to someone with COVID-19, you may become
                            infected, even if you feel well.
                        </p>
                        <p>After exposure to someone who has COVID-19, do the following:</p>
                        <ul>
                            <li>
                                Call your health care provider or COVID-19 hotline to find out where
                                and when to get a test.
                            </li>
                            <li>
                                Cooperate with contact-tracing procedures to stop the spread of the
                                virus.
                            </li>
                            <li>
                                If testing is not available, stay home and away from others for 14
                                days.
                            </li>
                            <li>
                                During this time, do not go to work, to school or to public places.
                                Ask someone to bring you supplies.
                            </li>
                            <li>
                                Keep at least a 1-metre distance from others, even from your family
                                members.
                            </li>
                            <li>
                                Wear a medical mask to protect others, including if/when you need to
                                seek medical care.
                            </li>
                            <li>Clean your hands frequently.</li>
                            <li>
                                Stay in a separate room from other family members, and if not
                                possible, wear a medical mask.
                            </li>
                            <li>Keep the room well-ventilated.</li>
                            <li>If you share a room, place beds at least 1 metre apart.</li>
                            <li>Monitor yourself for any symptoms for 14 days.&nbsp;</li>
                            <li>
                                Stay positive by keeping in touch with loved ones by phone or
                                online, and by exercising at home.
                            </li>
                        </ul>
                        <p>
                            If you live in an area with malaria or dengue fever, seek medical help
                            if you have a fever. While travelling to and from the health facility
                            and during medical care, wear a mask, keep at least a 1-metre distance
                            from other people and avoid touching surfaces with your hands. This
                            applies to adults and children.
                        </p>
                        <p>
                            <em>Read our </em>
                            <a href="/emergencies/diseases/novel-coronavirus-2019/question-and-answers-hub/q-a-detail/malaria-and-the-covid-19-pandemic">
                                <em>malaria and COVID-19 Q&amp;A</em>
                            </a>
                            <em> for more information.</em>
                        </p>
                    </Card>
                </TabPane>
                <TabPane tab="Treatment" key="3">
                    <Card>
                        <p>
                            Scientists around the world are working to find and develop treatments
                            for COVID-19.
                        </p>
                        <p>
                            Optimal supportive care includes oxygen for severely ill patients and
                            those who are at risk for severe disease and more advanced respiratory
                            support such as ventilation for patients who are critically ill.
                        </p>
                        <p>
                            Dexamethasone is a corticosteroid that can help reduce the length of
                            time on a ventilator and save lives of patients with severe and critical
                            illness.{" "}
                            <em>
                                Read our&nbsp;
                                <a href="/emergencies/diseases/novel-coronavirus-2019/question-and-answers-hub/q-a-detail/q-a-dexamethasone-and-covid-19">
                                    dexamethasone Q&amp;A
                                </a>{" "}
                                for more information.
                            </em>
                        </p>
                        <p>
                            Results from the WHO&rsquo;s Solidarity Trial{" "}
                            <a href="/news/item/15-10-2020-solidarity-therapeutics-trial-produces-conclusive-evidence-on-the-effectiveness-of-repurposed-drugs-for-covid-19-in-record-time">
                                indicated
                            </a>{" "}
                            that remdesivir, hydroxychloroquine, lopinavir/ritonavir and interferon
                            regimens appeared to have little or no effect on 28-day mortality or the
                            in-hospital course of COVID-19 among hospitalized patients.
                        </p>
                        <p>
                            Hydroxychloroquine has not been shown to offer any benefit for treatment
                            of COVID-19.{" "}
                            <em>
                                Read our&nbsp;
                                <a href="/emergencies/diseases/novel-coronavirus-2019/question-and-answers-hub/q-a-detail/q-a-hydroxychloroquine-and-covid-19">
                                    hydroxychloroquine Q&amp;A
                                </a>
                                &nbsp;for more information.
                            </em>
                        </p>
                        <p>
                            WHO does not recommend self-medication with any medicines, including
                            antibiotics, as a prevention or cure for COVID-19. WHO is coordinating
                            efforts to develop treatments for COVID-19 and will continue to provide
                            new information as it becomes available.
                        </p>
                    </Card>
                </TabPane>
            </Tabs>
        </Layout>
    );
};

export default HealthInfo;
