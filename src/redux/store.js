import { createStore, applyMiddleware } from "redux";
import thunk from "redux-thunk";
import logger from "redux-logger";
import rootReducer from "./reducer";

const middilewares = [thunk];

if (process.env.NODE_ENV === "development") {
    middilewares.push(logger);
}

// Note: this API requires redux@>=3.1.0
const store = createStore(rootReducer, applyMiddleware(...middilewares));

export default store;
