import axios from "axios";
import moment from "moment";
import {
    HIDE_LOADING,
    SET_COUNTRIES,
    SET_CURRENT_COUNTRY_DATA,
    SET_SUMMARY_DATA,
    SHOW_LOADING,
} from "./reducer";
import { START_TIME } from "../constants";

export const showLoading = () => ({
    type: SHOW_LOADING,
});

export const hideLoading = () => ({
    type: HIDE_LOADING,
});

/**
 * compare data util function
 *
 * @param data
 * @param index
 * @returns {number}
 */
const getNewData = (data, index, key) => {
    if (!data[index - 1]) return 0;

    if (data[index][key] < data[index - 1][key]) return 0;

    return data[index][key] - data[index - 1][key];
};

// For this small app, we don't split action creator to multiple files, one enough
/**
 * Fetching list of all countries
 *
 * @returns {function(...[*]=)}
 */
export const fetchCountriesList = () => (dispatch) => {
    dispatch(showLoading());
    axios
        .get("/countries")
        .then(({ data }) => {
            const dataInObj = {};
            data.forEach((country) => {
                dataInObj[country.ISO2] = country;
            });
            dispatch({
                type: SET_COUNTRIES,
                payload: dataInObj,
            });
        })
        .catch(() => {
            console.log("fetch error");
        })
        .finally(() => dispatch(hideLoading()));
};

/**
 * Fetching current country data by slug
 *
 * @param countrySlug
 * @returns {function(...[*]=)}
 */
export const fetchCountryData = (countrySlug) => (dispatch) => {
    dispatch(showLoading());

    axios
        .get("/total/dayone/country/" + countrySlug)
        .then(({ data }) => {
            dispatch({
                type: SET_CURRENT_COUNTRY_DATA,
                payload: data.map((item, index) => ({
                    ...item,
                    NewDeaths: getNewData(data, index, "Deaths"),
                    NewConfirmed: getNewData(data, index, "Confirmed"),
                    NewRecover: getNewData(data, index, "Recovered"),
                })),
            });
        })
        .catch(() => {
            console.log("fetch error");
        })
        .finally(() => dispatch(hideLoading()));
};

/**
 * Fetching summary data by global and each country
 *
 * @returns {function(*): Promise<unknown>}
 */
export const fetchSummary = () => (dispatch) => {
    dispatch(showLoading());

    Promise.all([
        axios.get("/summary"),
        axios.get("/world", {
            params: {
                from: START_TIME,
                to: new Date().toISOString(),
            },
        }),
    ])
        .then(([summaryData, wipData]) => {
            dispatch({
                type: SET_SUMMARY_DATA,
                payload: {
                    summary: summaryData.data,
                    wip: wipData.data,
                },
            });
        })
        .catch((error) => console.log(error))
        .finally(() => {
            dispatch(hideLoading());
        });
};
