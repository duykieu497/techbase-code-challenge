const initState = {
    loading: false,
    countries: {},
    countriesSummary: [],
    globalSummary: null,
    wip: [],
    currentCountriesData: [],
};

export const SHOW_LOADING = "SHOW_LOADING";
export const HIDE_LOADING = "HIDE_LOADING";
export const SET_COUNTRIES = "SET_COUNTRIES";
export const SET_SUMMARY_DATA = "SET_SUMMARY_DATA";
export const SET_CURRENT_COUNTRY_DATA = "SET_CURRENT_COUNTRY_DATA";

const reducer = (state = initState, action) => {
    switch (action.type) {
        case SHOW_LOADING:
            return {
                ...state,
                loading: true,
            };
        case HIDE_LOADING:
            return {
                ...state,
                loading: false,
            };
        // Set list of countries when finish fetching from API
        case SET_COUNTRIES:
            return {
                ...state,
                countries: action.payload,
            };
        case SET_SUMMARY_DATA:
            return {
                ...state,
                countriesSummary: action.payload.summary.Countries,
                globalSummary: action.payload.summary.Global,
                wip: action.payload.wip,
            };
        case SET_CURRENT_COUNTRY_DATA:
            return {
                ...state,
                currentCountriesData: action.payload,
            };
        default:
            return state;
    }
};

export default reducer;
