import React from "react";
import styled, { css } from "styled-components";
import { connect } from "react-redux";
import { AutoComplete } from "antd";
import { Link, useHistory } from "react-router-dom";
import {
    SearchOutlined,
    CloseOutlined,
    MenuFoldOutlined,
    MenuUnfoldOutlined,
} from "@ant-design/icons";
import { devices } from "../../styleConstants";
import Menu from "./Menu";

const Main = styled.div`
    padding-top: 8rem;
    max-width: 1366px;

    @media ${devices.sm} {
        padding-top: 6rem;
    }
`;

const Header = styled.div`
    display: flex;
    align-items: center;
    padding: 2rem;
    border-bottom: 1px solid #eee;
    position: fixed;
    top: 0;
    left: 0;
    width: 100%;
    z-index: 11;
    background-color: #fff;
    justify-content: space-between;
    height: 8rem;

    @media ${devices.sm} {
        height: 6rem;
    }

    & > div {
        display: flex;
        align-items: center;
    }

    .icon {
        cursor: pointer;
        margin-left: 1rem;
        svg {
            width: 2.5rem;
            height: 2.5rem;
            display: flex;
        }
    }
`;

const Logo = styled(Link)`
    display: block;
    height: 4.2rem;
    width: 20rem;
    background-image: url("/statics/logo.png");
    background-repeat: no-repeat;
    background-size: contain;
    text-indent: -300px;

    @media ${devices.xs} {
        ${(props) =>
            props.search &&
            css`
                display: none;
            `}
    }
`;

const PageTitle = styled.h1`
    font-size: 3rem;
    font-weight: 900;
    text-transform: uppercase;
    color: #464646;
    margin: 0;
    margin-left: 3rem;
    @media ${devices.xs} {
        display: none;
    }

    @media ${devices.sm} {
        ${(props) =>
            props.search &&
            css`
                display: none;
            `}
    }
`;

const StyledSearch = styled(AutoComplete)`
    @media ${devices.xs} {
        flex: 1;
    }
`;

const RightSide = styled.div`
    justify-content: flex-end;
    @media ${devices.xs} {
        flex: 1;
    }
`;

const Content = styled.div`
    display: flex;
    padding-left: 15rem;

    @media ${devices.md} {
        padding-left: 0;
    }
`;

const Nav = styled.div`
    width: 15rem;
    padding: 0;
    position: fixed;
    left: 0;
    height: 100%;
    padding-top: 15rem;
    z-index: 14;
    background-color: #fff;

    @media ${devices.md} {
        border-right: 1px solid #eee;
        ${(props) =>
            props.show
                ? css`
                      display: block;
                  `
                : css`
                      display: none;
                  `}
    }

    @media ${devices.sm} {
        padding-top: 13rem;
    }
    ul {
        padding: 0;
        display: flex;
        flex-direction: column;
        li {
            a {
                text-decoration: none;
                font-size: 1.5rem;
                padding: 0.8rem 0;
                padding-left: 2rem;
                padding-right: 1.5rem;
                color: #333;
                display: inline-block;
                transition: all 300ms;
                &:hover,
                &.active {
                    background-color: #eee;
                    border-bottom-right-radius: 1.5rem;
                    border-top-right-radius: 1.5rem;
                }
            }
        }
    }
`;

const Views = styled.div`
    flex: 1;
    padding: 2rem;
`;

const MenuButton = styled.button`
    cursor: pointer;
    background: transparent;
    display: none;
    margin-right: 1.5rem;
    border: 1px solid #dadada;
    font-size: 2rem;
    @media ${devices.md} {
        display: block;
    }
`;

const Overlay = styled.div`
    position: fixed;
    left: 0;
    top: 8rem;
    width: 100%;
    height: 100%;
    background-color: rgba(0, 0, 0, 0.3);
    z-index: 13;

    @media ${devices.sm} {
        top: 6rem;
    }
`;

const Layout = ({ children, search, countries }) => {
    const history = useHistory();

    const searchRef = React.createRef();

    // state which manage show search input
    const [showSearch, setShowSearch] = React.useState(false);

    // state manage show menu on mobile
    const [showNav, setShowNav] = React.useState(false);

    /**
     * Toggle show search input state
     */
    const toggleSearch = () => {
        setShowSearch((showSearch) => !showSearch);
    };

    /**
     * Focus on search when it state change to show
     */
    React.useEffect(() => {
        if (showSearch) {
            searchRef.current.focus();
        }
    }, [showSearch]);

    const onSelect = (value) => {
        if (countries[value]) {
            history.push("/country/" + value);
        }
    };

    return (
        <Main>
            <Header>
                <div>
                    <MenuButton
                        onClick={() => {
                            setShowNav(!showNav);
                        }}
                    >
                        {showNav ? <MenuFoldOutlined /> : <MenuUnfoldOutlined />}
                    </MenuButton>
                    <Logo search={showSearch ? "Y" : undefined} to="/">
                        Techbase
                    </Logo>
                    <PageTitle search={showSearch ? "Y" : undefined}>Covid 19 statics</PageTitle>
                </div>
                <RightSide>
                    {showSearch && (
                        <AutoComplete
                            id="searchCountries"
                            ref={searchRef}
                            style={{ width: 200 }}
                            options={Object.values(countries).map((country) => ({
                                ...country,
                                value: `${country.ISO2}`,
                                label: `${country.Country}`,
                            }))}
                            placeholder="Search countries"
                            filterOption={(inputValue, option) => {
                                return (
                                    option.value.toLowerCase().includes(inputValue) ||
                                    option.Slug.toLowerCase().includes(inputValue)
                                );
                            }}
                            onSelect={onSelect}
                            onChange={onSelect}
                        />
                    )}
                    {showSearch ? (
                        <CloseOutlined id="searchToggle" onClick={toggleSearch} className="icon" />
                    ) : (
                        <SearchOutlined id="searchToggle" onClick={toggleSearch} className="icon" />
                    )}
                </RightSide>
            </Header>
            <Content>
                <Nav show={showNav}>
                    <Menu />
                </Nav>
                <Views>
                    {showNav && <Overlay onClick={() => setShowNav(!showNav)} />}
                    {children}
                </Views>
            </Content>
        </Main>
    );
};

const mapStateToProps = ({ countries }) => ({
    countries,
});

export default connect(mapStateToProps)(Layout);
