import React from "react";
import { Link } from "react-router-dom";

const Menu = () => (
    <ul id="navMenu">
        <li>
            <Link to="/">Overview</Link>
        </li>

        <li>
            <Link to="/health-info">Health info</Link>
        </li>
        <li>
            <Link to="/faqs">FAQs</Link>
        </li>
    </ul>
);

export default Menu;
