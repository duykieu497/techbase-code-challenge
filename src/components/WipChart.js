import React from "react";
import PropTypes from "prop-types";
import { isMobile } from "react-device-detect";
import moment from "moment";
import { Line } from "react-chartjs-2";
import { chartColors } from "../utils";

const propTypes = {
    wipData: PropTypes.array.isRequired,
    title: PropTypes.string,
};

const defaultChartOptions = {
    responsive: true,
    aspectRatio: isMobile ? 1 : 2.5,
    hoverMode: "index",
    stacked: false,
    scales: {
        yAxes: [
            {
                type: "linear", // only linear but allow scale type registration. This allows extensions to exist solely for log scale for instance
                display: true,
                position: "left",
                id: "y-axis-1",
            },
            {
                type: "linear", // only linear but allow scale type registration. This allows extensions to exist solely for log scale for instance
                display: true,
                position: "right",
                id: "y-axis-2",

                // grid line settings
                gridLines: {
                    drawOnChartArea: false, // only want the grid lines for one axis to show up
                },
            },
        ],
    },
};

const WipChart = ({ wipData, title }) => {
    // Chart instance
    const [chartData, setChartData] = React.useState(null);

    //Chart options
    const [chartOptions, setChartOptions] = React.useState(defaultChartOptions);

    React.useEffect(() => {
        if (!title) return;

        setChartOptions((options) => ({ ...options, title: { display: true, title } }));
    }, [title]);

    /**
     * Update the state when props changed
     */
    React.useEffect(() => {
        if (Array.isArray(wipData)) {
            const infects = [];
            const deaths = [];
            const labels = [];
            wipData.forEach((item, index) => {
                infects.push(item.NewConfirmed);
                deaths.push(item.NewDeaths);
                labels.push(
                    item.Date ? moment(item.Date).utc().format("YYYY-MM-DD") : "T" + (index + 1)
                );
            });

            setChartData({
                labels,
                datasets: [
                    {
                        label: "Infect",
                        borderColor: chartColors.red,
                        backgroundColor: chartColors.red,
                        fill: false,
                        data: infects,
                        yAxisID: "y-axis-1",
                        pointRadius: 1,
                    },
                    {
                        label: "Death",
                        borderColor: chartColors.blue,
                        backgroundColor: chartColors.blue,
                        fill: false,
                        data: deaths,
                        yAxisID: "y-axis-2",
                        type: "bar",
                    },
                ],
            });
        }
    }, [wipData]);

    return <Line data={chartData} options={chartOptions} />;
};

WipChart.propTypes = propTypes;

export default WipChart;
