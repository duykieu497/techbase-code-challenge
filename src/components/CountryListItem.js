import React from "react";
import styled from "styled-components";
import { Card } from "antd";
import { Link } from "react-router-dom";
import PropTypes from "prop-types";

import { formatNumber } from "../utils";

const CountryName = styled.div`
    display: flex;
    align-items: center;
    font-size: 1.8rem;
    font-weight: bold;
    margin-bottom: 1rem;
    img {
        margin-right: 1rem;
    }
`;

const CountryRow = styled.div`
    display: flex;
    justify-content: space-between;
`;

const CountryLink = styled.div`
    display: flex;
    justify-content: space-between;
    margin-top: 2rem;
    span {
        font-size: 1.2rem;
        font-style: italic;
    }
`;

const propTypes = {
    country: PropTypes.object.isRequired,
};

const CountryListItem = ({ country }) => {
    return (
        <div key={country.CountryCode} className="col-sm-6 col-xl-4 country-list-item">
            <Card className="shadow" style={{ marginBottom: "3rem" }}>
                <CountryName>
                    <img src={`https://www.countryflags.io/${country.CountryCode}/shiny/32.png`} />
                    <div>{country.Country}</div>
                </CountryName>
                <CountryRow>
                    <span>Infected</span>
                    <span>{formatNumber(country.TotalConfirmed)}</span>
                </CountryRow>
                <CountryRow>
                    <span>Deaths</span>
                    <span>{formatNumber(country.TotalDeaths)}</span>
                </CountryRow>
                <CountryRow>
                    <span>Recovered</span>
                    <span>{formatNumber(country.TotalRecovered)}</span>
                </CountryRow>
                <CountryLink>
                    <Link to={`/country/${country.CountryCode}`}>Latest data 🔗</Link>
                    <span>Live update</span>
                </CountryLink>
            </Card>
        </div>
    );
};

CountryListItem.propTypes = propTypes;

export default CountryListItem;
