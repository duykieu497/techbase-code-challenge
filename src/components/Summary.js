import React from "react";
import styled from "styled-components";
import { Card } from "antd";
import PropTypes from "prop-types";

import { devices } from "../styleConstants";
import { formatNumber } from "../utils";

export const GlobalSummary = styled.div`
    .item {
        display: flex;
        align-items: flex-end;
        justify-content: center;
        @media ${devices.sm} {
            margin-bottom: 3rem;
        }
        & > div {
            flex: 1;
        }

        span {
            display: block;
            text-align: right;
            font-size: 2rem;
        }
        strong {
            font-weight: 700;
            display: block;
            text-align: right;
            font-size: 2.2rem;
        }
        .label {
            font-size: 1.4rem;
            font-weight: 500;
            margin-left: 1rem;
            width: 50%;
            padding-bottom: 0.4rem;
        }
    }
`;

const propTypes = {
    title: PropTypes.string.isRequired,
    data: PropTypes.object.isRequired,
};

const Summary = ({ title, data }) => {
    if (!data) return null;

    return (
        <React.Fragment>
            <h2>{title}</h2>
            <Card className="shadow" style={{ marginBottom: "2rem" }}>
                <GlobalSummary className="row" id="globalSummary">
                    <div className="col-sm-6 col-md-4">
                        <div className="item">
                            <div>
                                <span>{formatNumber(data.NewConfirmed)}</span>
                                <strong>{formatNumber(data.TotalConfirmed)}</strong>
                            </div>
                            <div className="label">Infected</div>
                        </div>
                    </div>
                    <div className="col-sm-6 col-md-4">
                        <div className="item">
                            <div>
                                <span>{formatNumber(data.NewDeaths)}</span>
                                <strong>{formatNumber(data.TotalDeaths)}</strong>
                            </div>
                            <div className="label">Deaths</div>
                        </div>
                    </div>
                    <div className="col-sm-6 col-md-4">
                        <div className="item">
                            <div>
                                <span>{formatNumber(data.NewRecovered)}</span>
                                <strong>{formatNumber(data.TotalRecovered)}</strong>
                            </div>
                            <div className="label">Recovered</div>
                        </div>
                    </div>
                </GlobalSummary>
            </Card>
        </React.Fragment>
    );
};

Summary.propTypes = propTypes;

export default Summary;
