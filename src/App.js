import React from "react";
import { connect } from "react-redux";
import Navigator from "./Navigator";

import "normalize.css/normalize.css";
import "antd/dist/antd.css";
import "./scss/main.scss";
import Loading from "./components/UI/Loading";
import { fetchCountriesList, fetchSummary } from "./redux/actions";

const mapStateToProps = ({ loading }) => ({ loading });

function App({ dispatch, loading }) {
    /**
     * Fetching countries list
     */
    React.useEffect(() => {
        dispatch(fetchCountriesList());
        dispatch(fetchSummary());
    }, []);

    return (
        <React.Fragment>
            <Loading visible={loading} />
            <Navigator />
        </React.Fragment>
    );
}

export default connect(mapStateToProps)(App);
